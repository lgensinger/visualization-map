import path from "path";
import { dirname } from "path";
import { fileURLToPath } from "url";

import { merge } from "webpack-merge";

const __dirname = dirname(fileURLToPath(import.meta.url));

import { commonConfig as visConfig } from "../webpack.common.js";
import { commonConfig } from "./webpack.common.js";

const webpackConfig = merge(visConfig, commonConfig, {

    devServer: {
        allowedHosts: ["all"],
        client: {
            webSocketURL: {
                hostname: "0.0.0.0",
                pathname: "/ws",
                password: "dev-server",
                port: 8080,
                protocol: "ws",
                username: "webpack",
            }
        },
        static: {
            directory: path.resolve(__dirname, "dist"),
            publicPath: "/"
        },
        watchFiles: {
            paths: ["example/**/*.*"],
            options: { usePolling: true },
        },
        webSocketServer: "ws"
    },

    mode: "development"

});

export { webpackConfig };
export default webpackConfig;