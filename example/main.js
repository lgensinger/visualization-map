import packageJson from "../package.json";
import { VisualizationMap } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

let data = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              -0.18311949782085435,
              51.524112572236476
            ],
            [
              -0.19328128598888838,
              51.511578089614886
            ],
            [
              -0.18542083599513148,
              51.4978346986118
            ],
            [
              -0.15282841947237102,
              51.505455119848165
            ],
            [
              -0.1558361451737369,
              51.52401252514309
            ],
            [
              -0.18311949782085435,
              51.524112572236476
            ]
          ]
        ],
        "type": "Polygon"
      }
    },
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              -0.11372536280845225,
              51.495850326660104
            ],
            [
              -0.11372536280845225,
              51.484758714307475
            ],
            [
              -0.09431987304824929,
              51.484758714307475
            ],
            [
              -0.09431987304824929,
              51.495850326660104
            ],
            [
              -0.11372536280845225,
              51.495850326660104
            ]
          ]
        ],
        "type": "Polygon"
      }
    },
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            [
              -0.1074645619268324,
              51.52894445636778
            ],
            [
              -0.10883628720753791,
              51.52166505153471
            ],
            [
              -0.10013275748593742,
              51.52599979138691
            ],
            [
              -0.1074645619268324,
              51.52894445636778
            ]
          ]
        ],
        "type": "Polygon"
      }
    }
  ]
};

// get elements
let container = document.getElementsByTagName("figure")[0];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // determine configs
    let width = container.offsetWidth;
    let height = width*0.75;

    // initialize
    const m = new VisualizationMap(null,data,width,height);

    // render visualization
    m.render(container);

}

// load document
document.onload = startup(data,container);

// attach events
container.outputContainer = outputContainer;
//container.addEventListener("bubble-click",processEvent);
//container.addEventListener("bubble-mouseover", processEvent);
//container.addEventListener("bubble-mouseout", processEvent);