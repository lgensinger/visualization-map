import packagejson from "../package.json";

const configuration = {
    branding: "lgv",
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationLayout = {
    height: process.env.LGV_HEIGHT || 400,
    tileserver: process.env.LGV_TILESERVER || "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    width: process.env.LGV_WIDTH || 400
}

export { configuration, configurationLayout };
export default configuration;
