import { ChartData } from "@lgv/visualization-chart";

/**
 * MapData is a data abstraction.
 * @param {array} data - geojson
 */
class MapData extends ChartData {

    constructor(data) {

        // initialize inheritance
        super(data);

    }

}

export { MapData };
export default MapData;
