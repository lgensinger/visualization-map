import { select } from "d3-selection";
import { LinearGrid } from "@lgv/visualization-chart";
import { mean } from "d3-array";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet/dist/images/marker-shadow.png";

import { configuration, configurationLayout } from "../configuration.js";

/**
 * VisualizationMap is a map abstraction.
 * @param {object} data - usually array; occasionally object
 * @param {node} dom - HTML dom node
 * @param {string} label - identifer for chart brand
 * @param {float} height - specified height of chart
 * @param {class} MapData - lgv data class
 * @param {string} name - name of chart
 * @param {float} width - specified width of chart
 */
class VisualizationMap extends LinearGrid {

    constructor(dom=null, data, width=configurationLayout.width, height=configurationLayout.height, MapData=null, tileServerUrl=configurationLayout.tileserver, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, MapData, label, name);

        let x = 0;
        let y = 0;

        if (this.data) {

            // determine x/y center based on provided data
            let x = mean(this.data.features, d => typeof(d.geometry.coordinates[0]) == "object" ? mean(d.geometry.coordinates.map(x => x[1]).flat()) : d.geometry.coordinates[1]);
            let y = mean(this.data.features, d => typeof(d.geometry.coordinates[0]) == "object" ? mean(d.geometry.coordinates.map(x => x[0]).flat()) : d.geometry.coordinates[0]);

        }
        
        // update self
        this.center = [x,y];
        this.debounceDelay = 500;
        this.map = null;
        this.moveendTimeout = null;
        this.tileServerUrl = tileServerUrl;
        this.zoom = 1;

        // if dom node provided set as the map container
        if (dom) this.container = select(dom);

    }

    /**
     * Determine if data is geojson feature collection.
     */
    get isFeatureCollection() {
        return this.data !== (null || undefined) && this.Data.source.features !== (null || undefined);
    }

    /**
     * Determine if data is geojson point.
     */
    get isPoint() {
        return this.data && this.data.coordinates && this.data.type.lower() == "point";
    }

    /**
     * Attach event handlers for map events.
     */
    attachEvents() {
        this.map.on("moveend", () => {
            clearTimeout(this.moveendTimeout);
            this.moveendTimeout = setTimeout(() => this.processMoveEnd(),this.debounceDelay);
        });
    }

    /**
     * Fit map tightly to data boundary.
     */
    fitToData() {

        // determine bounding box
        let bounds = (this.isFeatureCollection || this.isPoint) ? L.geoJSON(this.Data.source).getBounds() : this.map.getBounds();

        // fit to bounds
        if (this.isFeatureCollection) {
            // several data points
            this.map.fitBounds(bounds);
        } else if (this.isPoint) {
            // zoom out if only provided single data point
            this.map.setView(bounds.getCenter(), this.map.getMaxZoom() / 2);
        } else {
            // no geo data provided so center world
            this.map.setView(bounds.getCenter());
        }
    }

    /**
     * Generate main visualization, i.e. everything that sits inside the svg.
     */
    generateChart() {
        // empty method for specific map visualizations to overwrite
    }

    /**
     * Generate artboard for map.
     */
    generateCore() {

        // pull dom element styles
        let styles = window.getComputedStyle(this.container.node());
        let height = parseFloat(styles.height.split("px")[0]);
        let width = parseFloat(styles.width.split("px")[0]);

        // check for unset width or height
        if (height == 0 || width == 0) {

            // add dom element attribute
            this.container.node().setAttribute(`data-${this.branding}`, this.name);

            // generate css for account for unset dimension(s)
            let property = `[data-${this.branding}="${this.name}"]`;
            let value = width == 0 && height == 0 ? `{ height: ${this.height}px; width: ${this.width}px; }` : (width == 0 ? `{ width: ${this.width}px; }` : `{ height: ${this.height}px; }`);

            // generate stylesheet and insert in document
            let style = document.createElement("style");
            style.type = "text/css";
            style.append(document.createTextNode(`${property} ${value}`));
            document.head.append(style);

        }

        // update stored dimensions
        if (height > 0) this.heightSpecified = height;
        if (width > 0) this.widthSpecified = width;

        // initialize map
        this.map = L.map(this.container.node(), {
            center: this.center,
            zoom: this.zoom
        });

        // update tile server
        L.tileLayer(this.tileServerUrl).addTo(this.map);

    }

    /**
     * Generate visualization.
     */
    generateVisualization() {

        // generate core elements
        this.generateCore();

        // generate patterns
        this.generatePatterns();

        // add handlers for leaflet events
        this.attachEvents();

    }

    /**
     * Callback for map event on movened.
     */
    processMoveEnd() {
        this.updateMap();
    }

    /**
     * Update visualization.
     * @param {object} data - geojson
     * @param {float} height - specified height of chart
     * @param {float} width - specified width of chart
     */
    update(data, width, height) {

        // update self
        this.heightSpecified = height || this.heightSpecified;
        this.widthSpecified = width || this.widthSpecified;

        // if layout data object
        if (this.Data.height && this.Data.width) {

            // update layout attributes
            this.Data.height = this.heightSpecified;
            this.Data.width = this.widthSpecified;

        }

        // update data in class
        this.updateData(data);

        // generate visualization
        this.generateChart();

        // fit to bounds
        this.fitToData();

    }

    /**
     * Update visualization.
     * @param {object} data - geojson
     */
    updateData(data) {

        // recalculate values
        this.Data.source = data;
        this.Data.data = this.Data.update;
        this.data = this.Data.data;

    }

    /**
     * Update map state.
     */
    updateMap() {
console.log("FIRE!")
        // update dimensions on class
        let containerSize = this.map.getSize();
        this.heightSpecified = containerSize.y;
        this.widthSpecified = containerSize.x;

        // process data
        // update data object now that map exists
        this.Data.map = this.map;
        if (this.Data.data) this.data = this.Data.update;

        // render overlay
        this.generateChart();

        // broadcast event
        this.container.dispatch("map-change", {
            bubbles: true
        });

    }

}

export { VisualizationMap };
export default VisualizationMap;
