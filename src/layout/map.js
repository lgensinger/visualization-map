import { MapData } from "../structure/index.js";

/**
 * MapLayout is a data-bound layout abstraction for a leaflet map.
 * @param {array} data - geojson
 * @param {class} map - Leaflet map object
 */
class MapLayout extends MapData {

    constructor(data, map) {

        // initialize inheritance
        super(data);

        // update self
        this.map = map;

    }

}

export { MapLayout };
export default MapLayout;